var userCtl = require('../controllers/userCtrl');


module.exports = function(app) {
    app.post('/users', userCtl.create)
    app.get('/users', userCtl.list)
    app.get('/users/:uid', userCtl.getItem)
    app.delete('/users/:uid', userCtl.deleteItem)
    app.put('/users', userCtl.update)
    app.put('/users/reset/:uid',userCtl.resetPassword)
};
