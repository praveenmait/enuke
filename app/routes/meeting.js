'use strict';

var passport = require('passport'),
	meetingCtl = require('../controllers/meeting');

module.exports = function(app) {
  /**
   * Meeting Schedule
  */
  app.post('/meeting', meetingCtl.create)
  app.get('/meeting', meetingCtl.list)
  app.get('/meeting/all', meetingCtl.listAll)
  app.get('/meeting/:mid', meetingCtl.getItem)
  app.delete('/meeting/:mid', meetingCtl.deleteItem)
  app.put('/meeting/:mid', meetingCtl.update)
};
