'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    config = require('../../config/config'),
    ObjectId = Schema.ObjectId,
    async = require('async'),
    crypto = require('crypto');

// Meeting Schema

var MeetingSchema = new Schema({
    startDate :{
      type:Date,
      required: true,
    },
    startTime : {
      type:Date,
      required: true
    },
    duration : {
      type: Number,
      required: true
    },
    subject: {
      type : String,
      required: true
    },
    user:{
      type: ObjectId,
      ref: "Users"
    }
})

MeetingSchema.plugin(require('mongoose-timestamp'));
var MeetingSchema = mongoose.model('Meeting', MeetingSchema)
