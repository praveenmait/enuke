var mongoose = require('mongoose'),
    excelParser = require('excel'),
    async = require('async'),
    path =  require('path'),
    config = require('../../config/config'),
    Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    Server = require('mongodb').Server,
    Users = mongoose.model('Users'),
    _ = require('lodash'),
    fs = require('fs');


/*******************************
 ** Create account accessible **
 ** to admin only             **
 ******************************/

exports.create = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var newUser =  new Users(req.body)
    newUser.save(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
          })
    })
};



/*******************************
 ** List users **
 ** to admin only             **
 ******************************/


exports.list = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Users.find()
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
            'page': page,
            'perPage': perPage,
        })
    })
};


/*******************************
 ** Get user **
 ** to admin only             **
 ******************************/
// @body
// return message, resultMessage, results

exports.getItem = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Users.findById(req.params.uid)
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
        })
    })
};


/*******************************
 ** Update user **
 ** to admin only             **
 ******************************/


exports.update = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Users.update({
        '_id': req.body._id
    },{
        '$set':req.body
    })
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
        })
    })
};



/*******************************
 ** Delete user **
 ** to admin only             **
 ******************************/
// @body
// return message, resultMessage, results

exports.deleteItem = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Users.remove({
        '_id': req.body._id
    })
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
        })
    })
};


/**************************
 ** Change password   **
 **************************/
// Change password using this api
// @body newPassword, verifyPassword
// return errorcode or user
exports.resetPassword = function(req, res, next) {
    var passwordDetails = req.body;
    passwordDetails['uid'] = req.params.uid;
    var message = null;
    if (passwordDetails.newPassword) {
        Users.findById(passwordDetails.uid)
        .exec(function(err, user) {
            console.log('err in reset password', err)
            console.log('user in reset password', user)
            if (!err && user) {
                // console.log("passwordDetails.newPassword", passwordDetails.newPassword);
                // console.log("passwordDetails.verifyPassword", passwordDetails.verifyPassword);
                if (passwordDetails.newPassword === passwordDetails.verifyPassword) {
                    user.password = passwordDetails.newPassword;
                    user.save(function(err) {
                        if (err) {
                            return res.status(400).send({
                                message: getErrorMessage(err)
                            });
                        } else {
                            req.login(user, function(err) {
                                if (err) {
                                    res.send(400, err)
                                } else {
                                    // var locals = {
                                    //     email: user.email,
                                    //     subject: 'Reset Password',
                                    // };
                                    // Mailer.sendMail('passwordChange', locals, function(err, responseStatus, html, text) {
                                    //     if (err)
                                    //         console.log("error!!", err)
                                    // });
                                    res.send({
                                        'message': 'Password changed Successfully',
                                        'userType': user.userType
                                    });
                                }
                            });
                        }
                    });
                } else {
                    res.status(400).send({
                        message: 'Passwords do not match'
                    });
                }
            } else {
                res.status(400).send({
                    message: 'User does not exist'
                });
            }
        });
    } else {
        res.status(400).send({
            message: 'Please enter password'
        });
    }
}
