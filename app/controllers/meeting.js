var mongoose = require('mongoose'),
    Meeting = mongoose.model('Meeting');
    async = require('async'),
    Db = require('mongodb').Db,
    MongoClient = require('mongodb').MongoClient,
    assert = require('assert'),
    Server = require('mongodb').Server,
    Meeting = mongoose.model('Meeting'),
    _ = require('lodash'),
    fs = require('fs');


/*******************************
 ** Create meeting accessible **
 ******************************/
// @body startDate, duration, subject, startTime
// return MeetingID/Fail

exports.create = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var newMeeting =  new Meeting(req.body)
    newMeeting['user'] = req.user._id;
    newMeeting.save(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
          })
    })
};


/*******************************
 ** List Users All Meeting **
 ******************************/
// return response, resultMessage, results

exports.list = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Meeting.find({
      'user':req.user._id
    })
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
            'page': page,
            'perPage': perPage
        })
    })
};


/*******************************
 ** List All Meeting **
 ** Admin access **
 ******************************/
// return response

exports.listAll = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    var page = req.query.page || 1,
        perPage = req.query.perPage || 10;
    page =  parseInt(page);
    perPage = parseInt(perPage);
    page--;
    Meeting.find()
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
            'page': page,
            'perPage': perPage
        })
    })
};


/*******************************
 ** Get Meeting Detail **
 ******************************/
// return Response

exports.getItem = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    Meeting.findById(req.params.mid)
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        console.log("Params",req.params.mid)
        res.json({
            'response': data,
        })
    })
};


/*******************************
 ** Update Meeting  **
 ******************************/
// @body
// return Response

exports.update = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    Meeting.update({
        '_id': req.params.mid
    },{
        '$set':req.body
    })
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
        })
    })
};



/*******************************
 ** Delete user **
 ** to admin only             **
 ******************************/
// @body
// return Response

exports.deleteItem = function(req, res, next) {
    if (!req.user) {
      next({
        error: 'User must be signed in',
        code: 400
      });
    }
    Meeting.remove({
        '_id': req.param.mid
    })
    .exec(function(err,data){
        if(err) return next({
          error: err,
          code: 400
        })
        res.json({
            'response': data,
        })
    })

};
