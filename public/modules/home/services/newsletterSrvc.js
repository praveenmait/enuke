// angular.module('newsletter-app')
//
// .factory('Self',[ '$http' , function($http){
//     var selfData;
//     var selfDataClass = function(data){
//         angular.extend(this, data || {});
//     }
//     selfDataClass.prototype.set = function(key, value){
//         if(typeof key == 'object'){
//             angular.extend(selfData, key);
//         } else if (typeof key == 'string'){
//             selfData['key'] = value;
//         }
//     }
//     selfData = new selfDataClass({});
//     return selfData;
// }])
//
// .factory('newsletterhomeSrvc', ['$http' , function($http){
//     var data = function(){};
//
//     data.prototype.list = function(id, cb){
//         var self = this;
//         $http({
//             method :'GET',
//             url : '/category/'+id,
//         })
//         .success(function(data){
//             console.log(data.response);
//             angular.extend(self, data.response);
//             cb(null, self);
//         })
//         .error(function(err){
//             cb(err, null);
//         })
//         return this;
//     }
//     return new data();
// }])
//
//
// .factory('newsFilterSrvc', ['$http' , function($http){
//     var data = function(){};
//
//     data.prototype.filter = function(fltr,param, cb){
//         var self = this;
//         $http({
//             method :'GET',
//             url : '/filter',
//             params : {
//                 page : param.page,
//                 perPage : param.perPage,
//                 filter : fltr
//             }
//         })
//         .success(function(data){
//             angular.extend(self, data);
//             cb(null, self);
//         })
//         .error(function(err){
//             cb(err, null);
//         })
//         return this;
//     }
//     return new data();
// }])
//
// .factory('searchPetitionSrvc', ['$http' , function($http){
//     var data = function(){};
//
//     data.prototype.search = function(param, cb){
//         var self = this;
//         $http({
//             method :'GET',
//             url : '/search',
//             params : {
//                 'searchtext': param.searchtext,
//                 'page' : param.page,
//                 'perPage' : param.perPage
//             }
//         })
//         .success(function(data){
//             angular.extend(self, data);
//             cb(null, self);
//         })
//         .error(function(err){
//             cb(err, null);
//         })
//         return this;
//     }
//     return new data();
// }])
//
//
// .factory('searchByTextSrvc', ['$http' , function($http){
//     var data = function(){};
//
//     data.prototype.search = function(param, cb){
//         var self = this;
//         $http({
//             method :'GET',
//             url : '/search/all',
//             params : {
//                 'searchtext': param.searchtext,
//                 'page' : param.page,
//                 'perPage' : param.perPage
//             }
//         })
//         .success(function(data){
//             angular.extend(self, data);
//             cb(null, self);
//         })
//         .error(function(err){
//             cb(err, null);
//         })
//         return this;
//     }
//     return new data();
// }])
//
//
// .factory('filterByDateSrvc', ['$http' , function($http){
//     var data = function(){};
//
//     data.prototype.search = function(params, cb){
//         console.log('startdate', params.startdate, 'enddate', params.enddate);
//         var self = this;
//         $http({
//             method :'GET',
//             url : '/filterbydate',
//             params : {
//                 'searchdate': params.startdate,
//                 'enddate' : params.enddate,
//                 'page' : params.page,
//                 'perPage' : params.perPage
//             }
//         })
//         .success(function(data){
//             angular.extend(self, data);
//             cb(null, self);
//         })
//         .error(function(err){
//             cb(err, null);
//         })
//         return this;
//     }
//     return new data();
// }])
//
//
// .factory('segmentWiseSrvc', ['$http' , function($http){
//     var data = function(){};
//
//     data.prototype.categorylist = function(params, cb){
//         var self = this;
//         $http({
//             method :'GET',
//             url : '/segment',
//             params : {
//                 'segment': params.segment,
//                 'page' : params.page,
//                 'perPage' : params.perPage
//             }
//         })
//         .success(function(data){
//             angular.extend(self, data);
//             cb(null, self);
//         })
//         .error(function(err){
//             cb(err, null);
//         })
//         return this;
//     }
//     return new data();
// }])
//
//
// .factory('archiveListSrvc', ['$http' , function($http){
//     var data = function(){};
//
//     data.prototype.archivelist = function(params, cb){
//         var self = this;
//         $http({
//             method :'GET',
//             url : '/archive',
//             params : {
//                 'page' : params.page,
//                 'perPage' : params.perPage
//             }
//         })
//         .success(function(data){
//             angular.extend(self, data);
//             cb(null, self);
//         })
//         .error(function(err){
//             cb(err, null);
//         })
//         return this;
//     }
//     return new data();
// }])
