angular.module('home-app')

.config(['stateHelperProvider', '$locationProvider', '$urlRouterProvider', '$cookiesProvider', '$sceProvider',function(stateHelperProvider, $locationProvider, $urlRouterProvider, $cookiesProvider, $sceProvider){
  // console.log($sceProvider)
  // $sceProvider.enabled(false);
  $locationProvider.html5Mode(true);
  $urlRouterProvider.otherwise('/');
  stateHelperProvider

  .setNestedState({
            name: "login",
            url: '/',
            templateUrl: "templates/modules/home/views/login",
            controller: "loginCtrl",
        })
  .setNestedState({
            name: "register",
            url: '/register',
            templateUrl: "templates/modules/home/views/register",
            controller: "loginCtrl",
            module: 'public',
        })
  // home state
  .setNestedState({
      name: "home",
      url: 'home',
      templateUrl: "templates/modules/home/views/home",
      controller: "homeCtrl",
      children: [{
                name: "create",
                url: '/create',
                views: {
                    "meetingView": {
                        templateUrl: "templates/modules/home/views/createMeeting",
                        controller: "meetingCtrl",
                    }
                }
          },{
            name: "edit",
            url: '/edit',
            views: {
                "meetingView": {
                    templateUrl: "templates/modules/home/views/createMeeting",
                    controller: "meetingCtrl",
                }
            }
          },{
            name: "list",
            url: '/list?page&perPage',
            views: {
                "meetingView": {
                    templateUrl: "templates/modules/home/views/listMeeting",
                    controller: "meetingCtrl",
                }
            }
          },,{
            name: "listAll",
            url: '/listAll?page&perPage',
            views: {
                "meetingView": {
                    templateUrl: "templates/modules/home/views/listMeeting",
                    controller: "meetingCtrl",
                }
            }
          }]
      })
  // $locationProvider.html5Mode(true);
}])



// .run(['$state', '$cookies', '$rootScope' ,'Self', '$location', function($state, $cookies, $rootScope, Self,$location) {
//     // $rootScope.$on('$stateChangeStart', function(e, toState, toParams, fromState, fromParams) {
//     //     // console.log("xyz")
//     //     if (toState.module === 'public' && window.__user) {
//     //         e.preventDefault();
//     //         // If logged in and transitioning to a logged out page:
//     //         $state.go('home.home.news');
//     //     } else if (toState.module === 'private' && !window.__user) {
//     //         // If logged out and transitioning to a logged in page:
//     //         e.preventDefault();
//     //         $state.go('login');
//     //     };
//     // });
//     // $rootScope.$on("$locationChangeStart", function(event, next, current) {
//     //     console.log(current, event, next);
//     //     console.log('newsData', $rootScope.newsData);

//     //     // #!/fetch restricter
//     //     if(/#!\/fetch/.test(next)){
//     //         var id = next.split("cid=")[1];

//     //         _.forIn($rootScope.newsData, function (value, key) {
//     //             isAllowed =
//     //         })
//     //     }
//     // });
// }])
