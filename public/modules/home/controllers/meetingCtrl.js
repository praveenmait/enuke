angular.module('home-app')

.controller('meetingCtrl',['$scope', '$http', '$state', '$rootScope', function($scope, $http, $state, $rootScope){
  $scope.Meeting = {};
  $scope.MeetingData = {};
  $scope.create =  function(){
    $http.post('/meeting', $scope.Meeting)
      .success(function(data) {
          console.log("createdata",data)
          if (data.error) {
              var message = data.error.message
              alert(message)
          } else {
            $scope.MeetingData= data.response
            $state.go("home.list");
          }

      })
      .error(function(data) {
          console.log("error", data)
      });
  }

  $scope.edit =  function(){
    $http.put('/meeting', $scope.Meeting)
      .success(function(data) {
          if (data.error) {
              var message = data.error.message
              alert(message)
          } else {
            $scope.MeetingData= data
            $state.go("home.list");
          }

      })
      .error(function(data) {
          console.log("error", data)
      });
  }

  $scope.getItem =  function(mid){
    $http.get('/meeting?mid='+mid)
      .success(function(data) {
          if (data.error) {
              var message = data.error.message
              alert(message)
          } else {
            console.log("data getItem",data);
            $state.go("home.edit");
            $scope.Meeting= data.response;
          }

      })
      .error(function(data) {
          alert("error :"+data)
      });
  }

  $scope.list =  function(){
    $http.get('/meeting')
      .success(function(data) {
          if (data.error) {
              var message = data.error.message
              alert(message)
          } else {
            $scope.MeetingData= data.response
          }

      })
      .error(function(data) {
          console.log("error", data)
      });
  }
  $scope.list();
}])
