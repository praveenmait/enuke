angular.module('admin-app')



.controller('AdminLoginCtrl', ['$http', '$scope', '$modal', '$state', 'growl',
    function($http, $scope, $modal, $state, growl) {
        $scope.user = {};
        $scope.open = function() {
            var modalInstance = $modal.open({
                templateUrl: 'templates/modules/admin-app/views/forgotpassword',
                controller: 'forgetPassword',
                resolve: {
                    items: function() {
                        return $scope.email;
                    }
                }
            })
        }
        $scope.register = function(){
          $scope.user.type=2;
          $http.post('/user/register', $scope.user)
              .success(function(err, data) {
                  if (data==200) {
                      console.log("err", err)
                      growl.addSuccessMessage(JSON.stringify(err) + "data" + data);
                      $state.go('adminhome.list');
                  } else{
                    growl.addSuccessMessage("User successfully registered");
                  }

              })
              .error(function(err, data) {
                  console.log(JSON.stringify(err) + "data" + data)
                  growl.addErrorMessage("Bad Request: Username name already exists.");
              });
        }
        $scope.login = function() {
            $scope.user.type=1;
            $http.post('/user/login', $scope.user)
                .success(function(err, data) {
                    if (data==200 && err.name == 'Administrator') {
                        growl.addSuccessMessage(err.message);
                        $state.go('adminhome.list');
                    } else {
                        growl.addErrorMessage("You dont have permission.");
                    }

                })
                .error(function(err, data) {
                    growl.addErrorMessage("Invalid Credentials");
                });
        }
        $scope.logout = function(){
            $http.post('/users/logout')
                .success(function(data) {
                  delete window.__user
                  $state.go("login");
            })
        }
    }
])
